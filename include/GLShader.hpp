#ifndef SHADER_HPP
#define SHADER_HPP

#include <SDL2/SDL_opengl.h>
#include <string>
#include <fstream>
#include "GLException.hpp"

/*! \file */

///  The main namespace
namespace GL {

    /*! \enum GL::ShaderType
      Describes the set of types a particular shader can be.
     */
    enum class ShaderType {
	 /*! A vertex shader. */
	Vertex = GL_VERTEX_SHADER,
	/*! A fragment shader. */
	    Fragment = GL_FRAGMENT_SHADER, 
	/*! A geometry shader. */
	    Geometry = GL_GEOMETRY_SHADER, 
	/*! A tessallation control shader (OpenGL4 only). */
	    TessellationControl = GL_TESS_CONTROL_SHADER, 
	/*! A tessallation evaluation shader (OpenGL4 only). */
	    TessellationEvaluation = GL_TESS_EVALUATION_SHADER,
	/*! An invalid shader type. This value should only appear if there was an error related to shaders. */
	    Invalid 
    };

    /*! Write the type of a shader in an output stream.
     */
    std::ostream& operator<<(std::ostream& os, ShaderType t);

    /*! An exception thrown if loading a shader failed for some reason. */
    struct ShaderLoadingException : GLException{
	using GLException::GLException;
    };

    /*!RAII wrapper around a single GLSL shader of any type.
    It will accept source code as part of its constructors, and compile the program immediately.
    If the wrapped shader is freed outside of this class, using the wrapper object will have unknown consequences.
    */
    class Shader {
    public:
	/*!Constructs a Shader from an already existing shader name. The existing shader will be destroyed along with the object wrapping it
	  \exception ShaderLoadingException Thrown if the name does not refer to a valid shader.
	 */
	explicit Shader(GLuint shaderToWrap);    //If the shader was created outside this class, you can use this constructor to wrap it.
	                                        //If it's not a correct shader name, a ShaderLoadingException will be thrown.
	/*! Constructs a Shader from source code.
	  \param[in] ptrShaderCode The text making up the source code of the shader.
	  \param[in] shaderType    The type of the new shader.
	  \exception ShaderLoadingException Thrown if compiling the source code fails.
	*/
	explicit Shader(const char* ptrShaderCode, ShaderType shaderType);   //We're expecting the source code for this shader, it will be compiled immediately.
	/*! Create a new Shader with a copy of the source code of the original. The two copies will be completely independant of one another.
	 \exception ShaderLoadingException Thrown if for some reason the original Shader's source code is invalid, or if compiling the source code fails*/
        Shader(Shader const& shader);  //Will create a new shader with the same source code.
	                               //If somehow the original has no valid source, this will fail with a ShaderLoadingException.
	                               //If compiling fails, again, ShaderLoadingException.
	/*! Create a new Shader with a copy of the source code of the original.
	 \exception ShaderLoadingException Thrown if for some reason the original Shader's source code is invalid, or if compiling the source code fails*/
	Shader& operator=(Shader const&);  //The remarks for the copy constructor apply here too.
	/*! Move the Shader. The original object is left with a null name, and can't be used anymore. */
	Shader(Shader&&);
	/*! Move the Shader. The original object is left with a null name, and can't be used anymore. */
	Shader& operator=(Shader&&);
	/*! Free all resources associated with the shader. */
	~Shader();

	/*! Get the OpenGL name of the shader. */
	GLuint GetName(void) const noexcept;  //Don't try to delete its return value.
	/*! Implicit conversion of the Shader to its OpenGL name */
	operator GLuint(void) const noexcept;  //Implicitly convert this Shader to its OpenGL name.


    private:
	GLuint shader{};    //The handle to the shader.

	void LoadShader(const char* sourceCode, ShaderType type);
    };

    /*! Load a shader from a file in the file system.
      \param[in] filePath The path to the physical file
      \param[in] type     The type of shader that will be loaded
      \exception ShaderLoadingException Thrown if the file could not be loaded
    */
    Shader LoadShaderFromFile(std::string const& filePath, ShaderType type);   //Load a shader from a file in the file system. If loading the file fails, ShaderLoadingException
    /*! Create a new shader from another
      \param shaderToCopy The name of the shader to be copied
      \exception ShaderLoadingException Thrown if shaderToCopy is an invalid shader name
      \returns A new independant copy of the shader
    */
    Shader CopyShaderFromName(GLuint shaderToCopy);  //Couldn't make it a constructor as Shader(GLuint) was already taken for RAII wrapping
                                                   //May throw ShaderLoadingException if shaderToCopy is invalid.
    /*! Gather the source code for a shader.
      \param shader The name of the shader whose code is to be queried.
      \returns The source code as a string if the function is successful, or an empty string otherwise
    */
    std::string GetShaderSourceCode(GLuint shader); //Returns empty string if not a shader.
    /*! Get the type of a shader.
      \param shader The name of the shader whose type is to be queried.
      \returns The type of the shader
    */
    ShaderType GetShaderType(GLuint shader);  //Returns Invalid if not a shader.
    /*! Test whether an OpenGL name refers to a shader.
      \returns True if the name refers to a valid shader.
    */
    inline bool IsShader(GLuint shader) {
	return glIsShader(shader) == GL_TRUE;
    }
}

#endif //SHADER_HPP
