#ifndef PROGRAM_HPP
#define PROGRAM_HPP

#include "GLException.hpp"
#include <vector>

/// The main namespace
namespace GL {

    class Shader;

    /*! Thrown if there was a problem loading a program.
     */
    struct ProgramLoadingException : GLException {
	using GLException::GLException;   //Inherit constructor
    };

    /*! Thrown if there was a problem linking a program.
     */
    struct ProgramLinkingException : GLException {
	using GLException::GLException;   //Inherit constructor
    };

    /*! A RAII wrapper over an OpenGL program, manages its lifetime automatically. The wrapped program should not be freed outside of this class.
     */
    class Program {
    public:
	/*! Constructs a Program from an already existing program name.
	  \param[in] program The name of the program.
	  \throws ProgramLoadingException Thrown if the name does not refer to a valid program.
	*/
	explicit Program(GLuint program);   //Just pass an existing program to hold. ProgramLoadingException will be thrown should the name be invalid.
	/*! Constructs an empty Program with no shaders attached.
	 */
	Program(void);   //A lone program with no shaders attached initially.
	/*! Constructs a Program with a list of Shaders. The program will still have to be linked manually
	  \param[in] shaders The list of shaders to attach to the new program.
	*/
	explicit Program(std::vector<Shader> const& shaders);  //Construct the program with an initial list of shaders.
	                                                       //You'll still have to link it explicitly.
	/*! Create a copy of a Program by creating copies of its attached shaders. The linking status of the original program will be retained (that is, if the original program was linked, the new copy will automatically be linked)
	  \param[in] program The Program to create a copy of.
	 */
	Program(Program const& program);   //Create a copy of another program by copying its shaders and linking them if necessary.
	/*! Create a copy of a Program by creating copies of its attached shaders. The linking status of the original program will be retained (that is, if the original program was linked, the new copy will automatically be linked)
	  \param[in] program The Program to create a copy of.
	 */
	Program& operator=(Program const& program);  //See copy constr.
	/*! Move a Program. The original Program will refer to a null name: it can safely be destroyed but can't be used anymore.
	  \param[in] program The Program to be moved.
	 */
	Program(Program&& program);
	/*! Move a Program. The original Program will refer to a null name: it can safely be destroyed but can't be used anymore.
	  \param[in] program The Program to be moved.
	 */
	Program& operator=(Program&& program);
	/*! Free all resources associated with a Program.
	 */
	~Program();

	/*! Attach a set of shaders to a program. If you try to add a shader twice, it will be ignored.
	  \param[in] shaders The list of shaders to attach.
	*/
	void AttachShaders(std::vector<Shader> const& shaders) noexcept;  //Attach new shaders to the program. If any shaders were already added to the program, they will be not be attached again.
	/*! Link the attached shaders together.
	  \param[in] detachShadersAfterward Indicates whether all the shaders will be detached automatically after successful linking.
	  \exception ProgramLinkingException Thrown if linking failed.
	*/
	void Link(bool detachShadersAfterwards = true);   //Link the shaders together in a program. ProgramLinkingException is thrown upon failure.
	/*! Detach a set of attached shaders. If any shaders passed to the function were not actually attached to the program, they will be ignored.
	  \param[in] shaders The set of shaders to be detached.
	*/
	void DetachShaders(std::vector<Shader> const& shaders) noexcept;   //Detach a set of shaders from the program. Only the shaders actually attached to the program will be detached.

	/*! Convert implicitly the Program to the OpenGL name it is wrapping */
	operator GLuint(void) const noexcept;

    private:
	GLuint program{};

	std::vector<GLuint> GetAssociatedShaders(void) const noexcept;
	static std::vector<GLuint> ProgramAssociatedShaders(GLuint program);  //Get the shaders associated to a given program.
	std::string GetInfoLog(void) const noexcept;
    };

    /*! Checks whether an OpenGL name is a valid OpenGL program.
      \param[in] maybeProgram The OpenGL name to test.
      \returns True if the name refers to a valid program.
    */
    inline bool IsProgram(GLuint maybeProgram) {
	return glIsProgram(maybeProgram) == GL_TRUE;
    }
    /*! Checks whether a program was successfully linked
      \param[in] program The OpenGL name to check.
      \returns True if the program was successfully linked, false if linking was not successful or if the name is not a valid program.
    */
    inline bool ProgramLinkSuccess(GLuint program) {
	if (!IsProgram(program)) {
	    return false;
	}
	GLint status{};
	glGetProgramiv(program, GL_LINK_STATUS, &status);
	return status == GL_TRUE;
    }
}

#endif //PROGRAM_HPP
