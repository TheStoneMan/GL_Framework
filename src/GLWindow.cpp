#include "glew.h"
#include <SDL2/SDL_render.h>
#include "GLWindow.hpp"

using namespace std;

namespace GL {
    Window::Window(std::string const& name, int w, int h, int x, int y, int flags, int glMajVer, int glMinVer) :
	window{SDL_CreateWindow(name.c_str(), x, y, w, h, flags | SDL_WINDOW_OPENGL)}
    {
	//Set OpenGL version
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, glMajVer);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, glMinVer);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	context = SDL_GL_CreateContext(window);
	SDL_GL_MakeCurrent(window, context);
    }

    Window::~Window() {
	SDL_GL_DeleteContext(context);
	SDL_DestroyWindow(window);
    }

    void Window::Render(void) const noexcept {
	glFlush();
	SDL_GL_SwapWindow(window);
    }
}
