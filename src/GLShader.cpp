#include "glew.h"
#include <sstream>
#include <vector>
#include <utility>
#include "GLShader.hpp"

using namespace std;

namespace GL {

    ostream& operator<<(ostream& os, ShaderType t) {
	switch (t) {
	case ShaderType::Vertex:
	    return os << "vertex";
	case ShaderType::Fragment:
	    return os << "fragment";
	case ShaderType::Geometry:
	    return os << "geometry";
	case ShaderType::TessellationControl:
	    return os << "tessellation control";
	case ShaderType::TessellationEvaluation:
	    return os << "tessellation evaluation";
	default:
	    return os;
	}
    }

    
    Shader::Shader(GLuint shader)
    {
	if (!IsShader(shader)) {
	    throw ShaderLoadingException{"Error: the specified shader name doesn't actually refer to a valid shader!"};
	}
	this->shader = shader;
    }

    Shader::Shader(const char* src, ShaderType shaderType) {
	LoadShader(src, shaderType);
    }

    Shader::Shader(Shader const& shader) {
	//First we query the source code of the shader to copy.
	const string source { GetShaderSourceCode(shader.shader) };
	//We also query its type.
	const ShaderType typeShader { GetShaderType(shader.shader) };
	LoadShader(source.c_str(), typeShader);
    }

    Shader& Shader::operator=(Shader const& shader) {
	Shader tmp{shader};
	swap(*this, tmp);
	return *this;
    }

    //Good grief.
    Shader::Shader(Shader&& shader) {
	this->shader = shader.shader;
	shader.shader = 0;
    }

    Shader& Shader::operator=(Shader&& shader) {
	swap(this->shader, shader.shader);
	return *this;
    }

    Shader::~Shader() {
	glDeleteShader(shader);
    }

    GLuint Shader::GetName(void) const noexcept {
	return this->shader;
    }

    Shader::operator GLuint(void) const noexcept {
	return GetName();
    }

    void Shader::LoadShader(const char* sourceCode, ShaderType type) {
	shader = glCreateShader(static_cast<GLenum>(type));
	glShaderSource(shader, 1, &sourceCode, nullptr);  //Append the source code to the shader.
	glCompileShader(shader);

	//Check if compilation was successful.
	GLint status{};
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (status != GL_TRUE) {
	    //Not successful
	    //Now find out why.
	    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &status);
	    string log;
	    log.resize(static_cast<size_t>(status));  //have enough place to store the log.
	    glGetShaderInfoLog(shader, status, nullptr, &log[0]);
	    stringstream errorStream;
	    errorStream << "Error while compiling a " << type << " shader.\nError info: " << log;
	    throw ShaderLoadingException{errorStream.str()};
	}
    }

    Shader LoadShaderFromFile(string const& pathShader, ShaderType type) {
	ifstream fileShader{pathShader};
	if (!fileShader.is_open()) {
	    ostringstream ss;
	    ss << "Error: can't create the shader as it was impossible to open file : " << pathShader;
	    throw ShaderLoadingException{ss.str()};
	}
	ostringstream stringStream;
	stringStream << fileShader.rdbuf();  //Read the whole file as a string
	return Shader{stringStream.str().c_str(), type};
    }

    Shader CopyShaderFromName(GLuint shaderToCopy) {
	if (!IsShader(shaderToCopy)) {
	    throw ShaderLoadingException{"The name of the shader to copy doesn't actually refer to a valid shader!"};
	}
	const string sourceCode { GetShaderSourceCode(shaderToCopy) };
	const ShaderType type { GetShaderType(shaderToCopy) };
	return Shader{sourceCode.c_str(), type};
    }
    
    string GetShaderSourceCode(GLuint shader) {
	if (!IsShader(shader)) {
	    return "";
	}
 	GLint srcLength{};
	glGetShaderiv(shader, GL_SHADER_SOURCE_LENGTH, &srcLength);
	if (srcLength == 0) {
	    throw ShaderLoadingException{"The original shader has no source code."};
	}
	string source;
	source.resize(static_cast<size_t>(srcLength));
	glGetShaderSource(shader, srcLength, nullptr, &source[0]);
	return source;
   }

    ShaderType GetShaderType(GLuint shader) {
	if (!IsShader(shader)) {
	    return ShaderType::Invalid;
	}
	GLint type{};
	glGetShaderiv(shader, GL_SHADER_TYPE, &type);
	return static_cast<ShaderType>(type);
    }

}
