#include "glew.h"
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_render.h>
#include "GLEnvironment.hpp"

GL::Environment::Environment() {
    glewExperimental = GL_TRUE;   //Residue from one year ago. Useful or not? (SDL already initialised the GL version)
    glewInit();

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, depthSize);
}

GL::Environment::~Environment() {
    //Nothing
}
